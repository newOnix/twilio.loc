<?php

namespace App\Http\Controllers\CallFunctions;

use File;
use Config;
use Twilio\Twiml;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use App\Mail\SendToOperator;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class VoiceTwilioController extends Controller
{
    public function showWelcome(Request $request)
    {
		$response = new Twiml();

		if (!empty($request->Digits) || $request->Digits === '0') 
		{
		    switch ($request->Digits)
		    {
                case '0':
                    $response->play(asset('sound_call/0_number.mp3'));
                    $response->dial('+19374383700');
                    break;
			    case '1':
                    $response->play(asset('sound_call/0_number.mp3'));
			    	$dial = $response->dial(['action' => '/first_operator']);
					$dial->number("+19374691334");
			        break;
                case '2':
                    $response->play(asset('sound_call/record_message_anton.mp3'));
                    $response->record(['action' => '/record_action', 'maxLength' => 120, 'finishOnKey' => '#']);
                    break;
			    case '3':
                    $response->play(asset('sound_call/record_message_anton.mp3'));
                    $response->record(['action' => '/record_action', 'maxLength' => 120, 'finishOnKey' => '#']);
			        break;
                case '4':
                    $response->play(asset('sound_call/jennifer_new.mp3'));
                    $response->play(asset('sound_call/record_message_anton.mp3'));
                    $response->record(['action' => '/record_action', 'maxLength' => 120, 'finishOnKey' => '#']);
                    break;
			    default:
                    $response->redirect('/welcome');
		    }
		}
		else
		{
		    $gather = $response->gather(array('numDigits' => 1));
		    $gather->play(asset('sound_call/main.mp3'));
		    $response->redirect('/welcome');
		}

		echo $response;
    }

    public function callToFirstOperator(Request $request)
    {
        $response = new Twiml();
        $response->hangup();

        if($request->DialCallStatus == "busy" || $request->DialCallStatus == "no-answer" || $request->DialCallStatus == "no-answer")
        {
            Mail::to("mark@abcvoice.com")->send(new SendToOperator($request));
        }

        echo $response;
    }

    public function recordAction(Request $request)
    {
        $response = new Twiml();
        $gather = $response->gather(['finishOnKey' => '*', 'numDigits' => 1, 'action' => '/func_twilio/'. $request->RecordingSid . '/' . $request->AccountSid]);
        $gather->play(asset('sound_call/end_recording.mp3'), ['loop' => 3]);
        $response->redirect('/send_message/'. $request->RecordingSid . '/' . $request->AccountSid);

        echo $response;
    }

    public function playRecord(Request $request, $recordingSid, $accountSid)
    {
        $response = new Twiml();

        if(!empty($request->Digits)) 
        {
            switch ($request->Digits)
            {
                case '1':
                    $response->play('https://api.twilio.com/2010-04-01/Accounts/'. $accountSid .'/Recordings/' . $recordingSid);
                    $response->redirect('/func_twilio/'. $recordingSid . '/' . $accountSid);
                    break;
                case '2':
                    $sid    = "AC249b371be4f7156b392fcc7a9a6ae551";
                    $token  = "2e2a95d03bb65c869e51c9bff11da21f";
                    $twilio = new Client($sid, $token);
                    $twilio->recordings($recordingSid)->delete();
                    $response->play(asset('sound_call/record_message_anton.mp3'));
                    $response->record(['action' => '/record_action', 'maxLength' => 120, 'finishOnKey' => '#']);
                    break;
                case '#':
                    $response->redirect('/send_message/'. $recordingSid . '/' . $accountSid);
                    break;
                default:
                    $response->redirect('/func_twilio/'. $recordingSid . '/' . $accountSid);
            }
        }
        else
        {
            $gather = $response->gather(['finishOnKey' => '*', 'numDigits' => 1, 'action' => '/func_twilio/'. $recordingSid . '/' . $accountSid]);
            $gather->play(asset('sound_call/end_recording.mp3'), ['loop' => 3]);
            $response->redirect('/send_message/'. $recordingSid . '/' . $accountSid);
        }

        echo $response;
    }

    public function sendMessageOperator(Request $request, $recordingSid, $accountSid)
    {
        $response = new Twiml();
        $response->hangup();

        File::put(public_path() . '/message/' . $recordingSid .  ".mp3", 
            file_get_contents('https://api.twilio.com/2010-04-01/Accounts/'. $accountSid .'/Recordings/' . $recordingSid));

        Mail::to("mark@abcvoice.com")->send(new SendToOperator($request, $recordingSid));

        echo $response;
    }
}
