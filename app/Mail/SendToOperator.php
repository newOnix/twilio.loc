<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendToOperator extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $urlRecord=null)
    {
        $this->request = $request;
        $this->urlRecord = $urlRecord;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(!empty($this->urlRecord))
        {
            return $this->subject('Missed call')->view('emails.message_for_operator')
                                                ->with(['request' => $this->request,
                                                       ])
                                                ->attach(public_path() . '/message/' . $this->urlRecord .'.mp3');;
        }

        return $this->subject('Missed call')->view('emails.mail_for_operator')->with(['request' => $this->request]);
    }
}
