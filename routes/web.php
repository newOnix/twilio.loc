<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::any('welcome', 'CallFunctions\VoiceTwilioController@showWelcome')->name('welcome');
Route::any('first_operator', 'CallFunctions\VoiceTwilioController@callToFirstOperator')->name('first_operator');
Route::any('third_operator', 'CallFunctions\VoiceTwilioController@callToThirdOperator')->name('third_operator');
Route::any('record_action', 'CallFunctions\VoiceTwilioController@recordAction')->name('record_action');
Route::any('func_twilio/{recordingSid}/{accountSid}', 'CallFunctions\VoiceTwilioController@playRecord');
Route::any('send_message/{recordingSid}/{accountSid}', 'CallFunctions\VoiceTwilioController@sendMessageOperator');

